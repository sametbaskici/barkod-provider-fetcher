create sequence hibernate_sequence start 1 increment 1
create table card_meta (rev varchar(255) not null, collection varchar(255) not null, resource_id varchar(255), primary key (rev))
create table card_provider (id varchar(255) not null, height int4 not null, url varchar(255), width int4 not null, name varchar(255), detail_id int8, primary key (id))
create table card_provider_store_locations (card_provider_id varchar(255) not null, store_locations varchar(255))
create table card_provider_detail (id int8 not null, default_barcod_format varchar(255), input_type varchar(255), keyboard_type varchar(255), name varchar(255), visible_in_store_list boolean not null, website varchar(255), primary key (id))
create table card_provider_detail_common_search_items (card_provider_detail_id int8 not null, common_search_items varchar(255))
create table card_provider_detail_regions (card_provider_detail_id int8 not null, regions varchar(255))
alter table if exists card_meta add constraint FK4e6ootdo8rcjnxaip6qe8hnxx foreign key (resource_id) references card_provider
alter table if exists card_provider add constraint FKium7unj5krcumwsvvvgeadeb foreign key (detail_id) references card_provider_detail
alter table if exists card_provider_store_locations add constraint FKolatjr98midcshfrabvu5xr8 foreign key (card_provider_id) references card_provider
alter table if exists card_provider_detail_common_search_items add constraint FK3pvlvr4ey20m8ixubw748lk8d foreign key (card_provider_detail_id) references card_provider_detail
alter table if exists card_provider_detail_regions add constraint FK25y2yccm995nl6pin74vvoqqv foreign key (card_provider_detail_id) references card_provider_detail
create sequence hibernate_sequence start 1 increment 1
create table card_meta (rev varchar(255) not null, collection varchar(255) not null, resource_id varchar(255), primary key (rev))
create table card_provider (id varchar(255) not null, height int4 not null, url varchar(255), width int4 not null, name varchar(255), detail_id int8, primary key (id))
create table card_provider_store_locations (card_provider_id varchar(255) not null, store_locations varchar(255))
create table card_provider_detail (id int8 not null, default_barcod_format varchar(255), input_type varchar(255), keyboard_type varchar(255), name varchar(255), visible_in_store_list boolean not null, website varchar(255), primary key (id))
create table card_provider_detail_common_search_items (card_provider_detail_id int8 not null, common_search_items varchar(255))
create table card_provider_detail_regions (card_provider_detail_id int8 not null, regions varchar(255))
alter table if exists card_meta add constraint FK4e6ootdo8rcjnxaip6qe8hnxx foreign key (resource_id) references card_provider
alter table if exists card_provider add constraint FKium7unj5krcumwsvvvgeadeb foreign key (detail_id) references card_provider_detail
alter table if exists card_provider_store_locations add constraint FKolatjr98midcshfrabvu5xr8 foreign key (card_provider_id) references card_provider
alter table if exists card_provider_detail_common_search_items add constraint FK3pvlvr4ey20m8ixubw748lk8d foreign key (card_provider_detail_id) references card_provider_detail
alter table if exists card_provider_detail_regions add constraint FK25y2yccm995nl6pin74vvoqqv foreign key (card_provider_detail_id) references card_provider_detail
create sequence hibernate_sequence start 1 increment 1
create table card_meta (rev varchar(255) not null, collection varchar(255) not null, resource_id varchar(255), primary key (rev))
create table card_provider (id varchar(255) not null, height int4 not null, url varchar(255), width int4 not null, name varchar(255), detail_id int8, primary key (id))
create table card_provider_store_locations (card_provider_id varchar(255) not null, store_locations varchar(255))
create table card_provider_detail (id int8 not null, default_barcod_format varchar(255), input_type varchar(255), keyboard_type varchar(255), name varchar(255), visible_in_store_list boolean not null, website varchar(255), primary key (id))
create table card_provider_detail_common_search_items (card_provider_detail_id int8 not null, common_search_items varchar(255))
create table card_provider_detail_regions (card_provider_detail_id int8 not null, regions varchar(255))
alter table if exists card_meta add constraint FK4e6ootdo8rcjnxaip6qe8hnxx foreign key (resource_id) references card_provider
alter table if exists card_provider add constraint FKium7unj5krcumwsvvvgeadeb foreign key (detail_id) references card_provider_detail
alter table if exists card_provider_store_locations add constraint FKolatjr98midcshfrabvu5xr8 foreign key (card_provider_id) references card_provider
alter table if exists card_provider_detail_common_search_items add constraint FK3pvlvr4ey20m8ixubw748lk8d foreign key (card_provider_detail_id) references card_provider_detail
alter table if exists card_provider_detail_regions add constraint FK25y2yccm995nl6pin74vvoqqv foreign key (card_provider_detail_id) references card_provider_detail
create sequence hibernate_sequence start 1 increment 1
create table card_meta (rev varchar(255) not null, collection varchar(255) not null, resource_id varchar(255), primary key (rev))
create table card_provider (id varchar(255) not null, height int4 not null, url varchar(255), width int4 not null, name varchar(255), detail_id int8, primary key (id))
create table card_provider_store_locations (card_provider_id varchar(255) not null, store_locations varchar(255))
create table card_provider_detail (id int8 not null, default_barcod_format varchar(255), input_type varchar(255), keyboard_type varchar(255), name varchar(255), visible_in_store_list boolean not null, website varchar(255), primary key (id))
create table card_provider_detail_common_search_items (card_provider_detail_id int8 not null, common_search_items varchar(255))
create table card_provider_detail_regions (card_provider_detail_id int8 not null, regions varchar(255))
alter table if exists card_meta add constraint FK4e6ootdo8rcjnxaip6qe8hnxx foreign key (resource_id) references card_provider
alter table if exists card_provider add constraint FKium7unj5krcumwsvvvgeadeb foreign key (detail_id) references card_provider_detail
alter table if exists card_provider_store_locations add constraint FKolatjr98midcshfrabvu5xr8 foreign key (card_provider_id) references card_provider
alter table if exists card_provider_detail_common_search_items add constraint FK3pvlvr4ey20m8ixubw748lk8d foreign key (card_provider_detail_id) references card_provider_detail
alter table if exists card_provider_detail_regions add constraint FK25y2yccm995nl6pin74vvoqqv foreign key (card_provider_detail_id) references card_provider_detail
COPY card_provider_detail FROM '/Users/sametbaskici/Downloads/fetching-barkod-provider/src/main/resources/csv/part1/CARD_PROVIDER_DETAIL.csv' (FORMAT csv)
create sequence hibernate_sequence start 1 increment 1
create table card_meta (rev varchar(255) not null, collection varchar(255) not null, resource_id varchar(255), primary key (rev))
create table card_provider (id varchar(255) not null, height int4 not null, url varchar(255), width int4 not null, name varchar(255), detail_id int8, primary key (id))
create table card_provider_store_locations (card_provider_id varchar(255) not null, store_locations varchar(255))
create table card_provider_detail (id int8 not null, default_barcod_format varchar(255), input_type varchar(255), keyboard_type varchar(255), name varchar(255), visible_in_store_list boolean not null, website varchar(255), primary key (id))
create table card_provider_detail_common_search_items (card_provider_detail_id int8 not null, common_search_items varchar(255))
create table card_provider_detail_regions (card_provider_detail_id int8 not null, regions varchar(255))
alter table if exists card_meta add constraint FK4e6ootdo8rcjnxaip6qe8hnxx foreign key (resource_id) references card_provider
alter table if exists card_provider add constraint FKium7unj5krcumwsvvvgeadeb foreign key (detail_id) references card_provider_detail
alter table if exists card_provider_store_locations add constraint FKolatjr98midcshfrabvu5xr8 foreign key (card_provider_id) references card_provider
alter table if exists card_provider_detail_common_search_items add constraint FK3pvlvr4ey20m8ixubw748lk8d foreign key (card_provider_detail_id) references card_provider_detail
alter table if exists card_provider_detail_regions add constraint FK25y2yccm995nl6pin74vvoqqv foreign key (card_provider_detail_id) references card_provider_detail
COPY card_provider_detail FROM '/Users/sametbaskici/Downloads/fetching-barkod-provider/src/main/resources/csv/part1/CARD_PROVIDER_DETAIL.csv' (FORMAT csv)
