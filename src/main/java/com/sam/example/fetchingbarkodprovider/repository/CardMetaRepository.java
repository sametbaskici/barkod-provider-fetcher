package com.sam.example.fetchingbarkodprovider.repository;

import com.sam.example.fetchingbarkodprovider.domain.CardMeta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CardMetaRepository extends JpaRepository<CardMeta, String> {
}
