package com.sam.example.fetchingbarkodprovider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FetchingBarkodProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(FetchingBarkodProviderApplication.class, args);
    }

}

