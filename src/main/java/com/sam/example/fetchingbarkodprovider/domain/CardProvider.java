package com.sam.example.fetchingbarkodprovider.domain;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Builder
@Data
@Entity
public class CardProvider {
    @Id
    @Column(unique = true, updatable = false, nullable = false)
    private String id;
    private String name;
    @Embedded
    private CardLogo logo;

    @ElementCollection
    private List<String> storeLocations;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "detail_id")
    private CardProviderDetail detail;
}
