package com.sam.example.fetchingbarkodprovider.domain;

import lombok.*;

import javax.persistence.Embeddable;

@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Builder
@Data
@Embeddable
public class CardLogo {
    private String url;
    private int width;
    private int height;
}
