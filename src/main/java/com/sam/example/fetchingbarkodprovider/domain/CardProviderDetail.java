package com.sam.example.fetchingbarkodprovider.domain;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Builder
@Data
@Entity
public class CardProviderDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ElementCollection
    private List<String> commonSearchItems;
    private String defaultBarcodFormat;
    private String inputType;
    private String keyboardType;
    private String name;
    @ElementCollection
    private List<String> regions;
    private boolean visibleInStoreList;
    private String website;

}
