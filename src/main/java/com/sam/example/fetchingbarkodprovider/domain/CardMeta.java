package com.sam.example.fetchingbarkodprovider.domain;

import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Builder
@Entity
public class CardMeta {

    @Id
    @Column(unique = true, updatable = false, nullable = false)
    private String rev;

    @Column(nullable = false, updatable = false)
    private String collection;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "resource_id")
    private CardProvider provider;

}
