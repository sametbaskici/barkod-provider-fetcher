package com.sam.example.fetchingbarkodprovider.api.model;

import lombok.Data;

import java.util.List;

@Data
public class CardProviderDetailDTO {
    private List<String> common_search_terms;
    private String default_barcode_format;
    private String input_type;
    private String keyboard_type;
    private String name;
    private List<String> regions;
    private boolean visible_in_store_list;
    private String website;
}
