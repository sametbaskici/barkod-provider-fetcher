package com.sam.example.fetchingbarkodprovider.api.client;


import com.sam.example.fetchingbarkodprovider.api.model.CardMetasDTO;
import com.sam.example.fetchingbarkodprovider.api.model.CardProviderDTO;
import com.sam.example.fetchingbarkodprovider.api.model.CardProviderDetailDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;

@Component
@RequiredArgsConstructor
public class LoyaltyCardClient {

    private static final String API_KEY = "Basic M2RhM2ZhYzEtMzRmZS00NzU1LThkOTUtOTVjOTFlMzg3MDMyOmExZDgyYzRlLTg3ZjMtNDUyNi04NjY1LTEyNTc4ZTQ0ZWM2Zg==";
    private static final String BASE_URL = "https://cdn-sync-service-client-prod.stocard-backend.com";
    private static final String PROVIDER_BASE_URL = "https://mb-cdn.stocard.de";
    private static final String META_URL = "/meta-buckets/global?";
    private static final String PROVIDER_URL = "/providers";
    private static final String PROVIDER_DETAIL_URL = "/loyalty-card-providers";

    private RestTemplate restTemplate;
    private RestTemplate providerRestTemplate;
    private final RestTemplateBuilder restTemplateBuilder;

    @PostConstruct
    public void init() {
        restTemplate = restTemplateBuilder
                .rootUri(BASE_URL)
                .build();

        providerRestTemplate = restTemplateBuilder
                .rootUri(PROVIDER_BASE_URL)
                .build();
    }

    public CardMetasDTO fetchCardMeta() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("user-agent",UserAgentLookup.getRandomAgent());
        headers.set("Authorization", API_KEY);
        HttpEntity request = new HttpEntity(headers);
        ResponseEntity<CardMetasDTO> responseEntity = restTemplate.exchange(META_URL, HttpMethod.GET, request, CardMetasDTO.class);
        return responseEntity.getBody();
    }

    public CardProviderDTO fetchCardProvider(String resourceId) {
        String url = PROVIDER_URL.concat("/").concat(resourceId);
        HttpHeaders headers = new HttpHeaders();
        headers.set("user-agent",UserAgentLookup.getRandomAgent());

        HttpEntity request = new HttpEntity(headers);
        ResponseEntity<CardProviderDTO> responseEntity = providerRestTemplate.exchange(url, HttpMethod.GET, request, CardProviderDTO.class);
        return responseEntity.getBody();
    }

    public CardProviderDetailDTO fetchCardProviderDetail(String resourceId) {
        String url = PROVIDER_DETAIL_URL.concat("/").concat(resourceId);
        HttpHeaders headers = new HttpHeaders();
        headers.set("user-agent",UserAgentLookup.getRandomAgent());
        HttpEntity request = new HttpEntity(headers);
        ResponseEntity<CardProviderDetailDTO> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, CardProviderDetailDTO.class);
        return responseEntity.getBody();
    }

}
