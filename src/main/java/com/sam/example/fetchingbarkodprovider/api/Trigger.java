package com.sam.example.fetchingbarkodprovider.api;

import com.sam.example.fetchingbarkodprovider.api.client.LoyaltyCardClient;
import com.sam.example.fetchingbarkodprovider.api.converter.CardMetaConverter;
import com.sam.example.fetchingbarkodprovider.api.model.CardMetaDTO;
import com.sam.example.fetchingbarkodprovider.api.model.CardMetasDTO;
import com.sam.example.fetchingbarkodprovider.api.model.CardProviderDTO;
import com.sam.example.fetchingbarkodprovider.api.model.CardProviderDetailDTO;
import com.sam.example.fetchingbarkodprovider.domain.CardMeta;
import com.sam.example.fetchingbarkodprovider.repository.CardMetaRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpServerErrorException;

import java.net.SocketException;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@RequestMapping("trigger")
@RestController
public class Trigger {

    private final LoyaltyCardClient client;
    private final CardMetaConverter cardMetaConverter;
    private final CardMetaRepository cardMetaRepository;
    private Random random = new Random();

    @GetMapping
    public void triggerToLoadAllData() {
        CardMetasDTO cardMetasDTO = client.fetchCardMeta();
        fixMetasResourceId(cardMetasDTO);

        int count = 1;
        for (CardMetaDTO metaDTO : cardMetasDTO.getMetas()) {
            try {
                Thread.sleep(getEstimatedWaitingTime());

                CardProviderDTO providerDTO = client.fetchCardProvider(metaDTO.getResource_id());
                CardProviderDetailDTO providerDetailDTO = client.fetchCardProviderDetail(metaDTO.getResource_id());

                log.info("card provider successfully fetched : {}", providerDTO);
                log.info("card provider detail successfully fetched : {}", providerDetailDTO);

                CardMeta cardMeta = cardMetaConverter.convert(metaDTO, providerDTO, providerDetailDTO);
                cardMetaRepository.save(cardMeta);

                log.info("{}. card meta processed.", count);
                count++;
            } catch (InterruptedException e) {
                log.error("interruptedException for this card meta : {}", metaDTO.getResource_id());
            }catch (HttpServerErrorException e){
                log.error("Http Server Error Exception for thi meta : {}", metaDTO.getResource_id());
            }catch (Exception e){
                log.error("Exception for this meta : {} \nexception: {}", metaDTO.getResource_id(), e.getMessage());
            }
        }
    }

    private int getEstimatedWaitingTime() {
        return random.nextInt(500) + 300;
    }


    private void fixMetasResourceId(CardMetasDTO metas){
        List<CardMetaDTO> sorted = metas.getMetas().stream()
                .peek(metaDTO -> fixMetaResourceId(metaDTO))
                .sorted(Comparator.comparing(CardMetaDTO::getRev))
                .collect(Collectors.toList());
        metas.setMetas(sorted);
    }

    private void fixMetaResourceId(CardMetaDTO metaDTO) {
        try {
            Integer.parseInt(metaDTO.getResource_id());
        } catch (NumberFormatException e) {
            metaDTO.setResource_id(metaDTO.getCollection().split("/")[2]);
        }
    }


}
