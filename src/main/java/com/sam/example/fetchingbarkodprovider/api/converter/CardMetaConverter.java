package com.sam.example.fetchingbarkodprovider.api.converter;

import com.sam.example.fetchingbarkodprovider.api.model.CardMetaDTO;
import com.sam.example.fetchingbarkodprovider.api.model.CardProviderDTO;
import com.sam.example.fetchingbarkodprovider.api.model.CardProviderDetailDTO;
import com.sam.example.fetchingbarkodprovider.domain.CardMeta;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CardMetaConverter {

    private final CardProviderConverter providerConverter;

    public CardMeta convert(CardMetaDTO metaDTO, CardProviderDTO providerDTO, CardProviderDetailDTO providerDetailDTO) {
        return CardMeta.builder()
                .rev(metaDTO.getRev())
                .collection(metaDTO.getCollection())
                .provider(providerConverter.convert(providerDTO, providerDetailDTO))
                .build();
    }
}
