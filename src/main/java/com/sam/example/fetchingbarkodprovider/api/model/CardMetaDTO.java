package com.sam.example.fetchingbarkodprovider.api.model;

import lombok.Data;

@Data
public class CardMetaDTO {
    private String rev;
    private String collection;
    private String resource_id;
}
