package com.sam.example.fetchingbarkodprovider.api;

import com.sam.example.fetchingbarkodprovider.api.client.LoyaltyCardClient;
import com.sam.example.fetchingbarkodprovider.api.model.CardMetasDTO;
import com.sam.example.fetchingbarkodprovider.api.model.CardProviderDTO;
import com.sam.example.fetchingbarkodprovider.api.model.CardProviderDetailDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/loyalty")
@RequiredArgsConstructor
public class TestController {

    private final LoyaltyCardClient client;

    @GetMapping("/metas")
    public CardMetasDTO getMetas() {
        return client.fetchCardMeta();
    }

    @GetMapping("/provider/{resourceId}")
    public CardProviderDTO getProvider(@PathVariable String resourceId) {
        return client.fetchCardProvider(resourceId);
    }

    @GetMapping("/provider-detail/{resourceId}")
    public CardProviderDetailDTO getDetail(@PathVariable String resourceId) {
        return client.fetchCardProviderDetail(resourceId);
    }




}
