package com.sam.example.fetchingbarkodprovider.api.model;

import lombok.Data;

import java.util.List;

@Data
public class CardMetasDTO {
    private List<CardMetaDTO> metas;
    private String id;
}
