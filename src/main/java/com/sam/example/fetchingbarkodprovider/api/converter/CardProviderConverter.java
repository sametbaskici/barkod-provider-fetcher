package com.sam.example.fetchingbarkodprovider.api.converter;

import com.sam.example.fetchingbarkodprovider.api.model.CardLogoDTO;
import com.sam.example.fetchingbarkodprovider.api.model.CardProviderDTO;
import com.sam.example.fetchingbarkodprovider.api.model.CardProviderDetailDTO;
import com.sam.example.fetchingbarkodprovider.domain.CardLogo;
import com.sam.example.fetchingbarkodprovider.domain.CardProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CardProviderConverter {

    private final CardProviderDetailConverter cardProviderDetailConverter;

    public CardProvider convert(CardProviderDTO providerDTO, CardProviderDetailDTO providerDetailDTO) {
        return CardProvider.builder()
                .id(providerDTO.getId())
                .name(providerDetailDTO.getName())
                .logo(convertLogo(providerDTO.getLogo()))
                .storeLocations(providerDTO.getStoreLocations())
                .detail(cardProviderDetailConverter.convert(providerDetailDTO))
                .build();
    }

    public CardLogo convertLogo(CardLogoDTO logoDTO) {
        return CardLogo.builder()
                .url(logoDTO.getUrl())
                .width(logoDTO.getWidth())
                .height(logoDTO.getHeight())
                .build();

    }

}
