package com.sam.example.fetchingbarkodprovider.api.model;

import lombok.Data;

import java.util.List;

@Data
public class CardProviderDTO {
    private String id;
    private String name;
    private CardLogoDTO logo;
    private List<String> storeLocations;
}
