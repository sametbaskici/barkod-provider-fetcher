package com.sam.example.fetchingbarkodprovider.api.model;

import lombok.Data;

@Data
public class CardLogoDTO {
    private String url;
    private int width;
    private int height;
}
