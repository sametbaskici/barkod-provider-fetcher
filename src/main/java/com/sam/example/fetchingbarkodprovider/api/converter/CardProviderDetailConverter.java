package com.sam.example.fetchingbarkodprovider.api.converter;

import com.sam.example.fetchingbarkodprovider.api.model.CardProviderDetailDTO;
import com.sam.example.fetchingbarkodprovider.domain.CardProviderDetail;
import org.springframework.stereotype.Component;

@Component
public class CardProviderDetailConverter {

    public CardProviderDetail convert(CardProviderDetailDTO providerDetailDTO){
        return CardProviderDetail.builder()
                .commonSearchItems(providerDetailDTO.getCommon_search_terms())
                .defaultBarcodFormat(providerDetailDTO.getDefault_barcode_format())
                .inputType(providerDetailDTO.getInput_type())
                .keyboardType(providerDetailDTO.getKeyboard_type())
                .name(providerDetailDTO.getName())
                .regions(providerDetailDTO.getRegions())
                .visibleInStoreList(providerDetailDTO.isVisible_in_store_list())
                .website(providerDetailDTO.getWebsite())
                .build();
    }

}
